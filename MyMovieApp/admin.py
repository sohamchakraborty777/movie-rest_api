from django.contrib import admin
from .models import MovieTable

# Register your models here.
class MovieTableAdmin(admin.ModelAdmin):

    list_display = ('id', 'movie_name', 'release_date')

admin.site.register(MovieTable, MovieTableAdmin)