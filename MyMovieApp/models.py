from django.db import models

# Create your models here.
class MovieTable(models.Model):
    movie_name = models.CharField(max_length=25, db_column='Name')
    movie_desc = models.TextField(max_length=250, db_column='Description')
    release_date = models.DateField()

    def __str__(self):
        return self.movie_name