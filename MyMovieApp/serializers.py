from rest_framework import serializers
from .models import MovieTable

class MovieSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = MovieTable
        fields = ('id', 'movie_name', 'movie_desc', 'release_date')
        
        extra_kwargs = {
            'movie_desc': {'write_only': True},
            'release_date': {'write_only': True}
        }

class GetMovieSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = MovieTable
        fields = ('id', 'movie_name', 'movie_desc', 'release_date')
        
        