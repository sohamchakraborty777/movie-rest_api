from rest_framework.test import APITestCase
from django.urls import reverse

class TestSetup(APITestCase):

    def setUp(self):
        self.post_list_movie_url = reverse('post_list_movie')

        self.data = {
                "movie_name":"Titanic",
                "movie_desc":"Titanic is a 1997 American epic romance and disaster film.",
                "release_date":"1997-12-17"
                }

        self.updated_data = {
                "movie_name":"Titanic: Movie",
                "movie_desc":"Titanic is a 1997 American epic romance and disaster film.",
                "release_date":"1997-12-17"
                }

        return super().setUp()