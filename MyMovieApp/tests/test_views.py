from .test_setup import TestSetup

class TestViews(TestSetup):

    def test_add_movie_failed(self):
        res = self.client.post(self.post_list_movie_url)
        self.assertEqual(res.status_code, 400)

    def test_add_movie_success(self):
        res = self.client.post(self.post_list_movie_url, self.data, format="json")
        self.assertEqual(res.data['movie_name'], self.data['movie_name'])
        self.movie_id = res.data['id']
        self.assertEqual(res.status_code, 201)
        return res.data['id']

    def test_movie_list_success(self):
        self.test_add_movie_success()
        res = self.client.get(self.post_list_movie_url)
        self.assertEqual(res.status_code, 200)

    def test_get_movie_failed(self):
        movie_id = self.test_add_movie_success()
        res = self.client.get(self.post_list_movie_url+"70")
        self.assertEqual(res.status_code, 404)

    def test_get_movie_success(self):
        movie_id = self.test_add_movie_success()
        res = self.client.get(self.post_list_movie_url+str(movie_id))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['movie_name'], self.data['movie_name'])

    def test_update_movie_failed_1(self):
        movie_id = self.test_add_movie_success()
        res = self.client.put(self.post_list_movie_url+"70", self.updated_data, format="json")
        self.assertEqual(res.status_code, 404)

    def test_update_movie_failed_2(self):
        movie_id = self.test_add_movie_success()
        res = self.client.put(self.post_list_movie_url+str(movie_id))
        self.assertEqual(res.status_code, 400)

    def test_update_movie_success(self):
        movie_id = self.test_add_movie_success()
        res = self.client.put(self.post_list_movie_url+str(movie_id), self.updated_data, format="json")
        self.assertEqual(res.status_code, 200)

    def test_delete_movie_failed(self):
        movie_id = self.test_add_movie_success()
        res = self.client.delete(self.post_list_movie_url+"70")
        self.assertEqual(res.status_code, 404)

    def test_delete_movie_success(self):
        movie_id = self.test_add_movie_success()
        res = self.client.delete(self.post_list_movie_url+str(movie_id))
        self.assertEqual(res.status_code, 200)