from django.urls import path
import MyMovieApp.views as api_views
# from drf_yasg2.views import get_schema_view
# from drf_yasg2 import openapi
from rest_framework import permissions


# app_name = "MovieApi"

# schema_view = get_schema_view(
#    openapi.Info(
#       title="Snippets API",
#       default_version='v1',
#       description="Test description",
#       terms_of_service="https://www.google.com/policies/terms/",
#       contact=openapi.Contact(email="contact@snippets.local"),
#       license=openapi.License(name="BSD License"),
#    ),
#    public=True,
#    permission_classes=(permissions.AllowAny,),
# )


urlpatterns = [
    #########################Swagger Documentation for testing purpose#########################
   #  path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   #  path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('movie/<int:id>', api_views.MovieView.as_view({'get':'get_movie','put':'update_movie','delete':'delete_movie'}), name='get_put_delete_movie'),
    path('movie/', api_views.MovieView.as_view({'post':'add_movie','get':'movie_list'}), name='post_list_movie'),
]
