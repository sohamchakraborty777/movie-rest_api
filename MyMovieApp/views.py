from rest_framework.response import Response
from rest_framework import status,viewsets
from django.shortcuts import get_object_or_404
from .models import MovieTable
from .serializers import MovieSerializer,GetMovieSerializer

# Create your views here.
class MovieView(viewsets.ViewSet):

    def movie_list(self, request):
        movie_obj = MovieTable.objects.all()
        serializer = GetMovieSerializer(movie_obj, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_movie(self, request, id):
        movie_obj = get_object_or_404(MovieTable, id=id)
        serializer = GetMovieSerializer(movie_obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def add_movie(self, request):
        serializer = MovieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update_movie(self, request, id):
        movie_obj = get_object_or_404(MovieTable, id=id)
        serializer = MovieSerializer(movie_obj, data=request.data, partial=True)
        if serializer.is_valid() and len(request.data) != 0:
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete_movie(self, request, id):
        try:
            movie_obj = MovieTable.objects.get(id=id)
            serializer = MovieSerializer(movie_obj)
            serializer_data = serializer.data
            movie_obj.delete()
            return Response(serializer_data, status=status.HTTP_200_OK)

        except MovieTable.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)